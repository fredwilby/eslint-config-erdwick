module.exports = {
  extends: 'airbnb',
  plugins: [
    'react',
  ],
  env: { browser: true, },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      experimentalObjectRestSpread: true,
    },
  },
    /* airbnb overrides */
  rules: {
    'import/extensions': 0,
    'react/jsx-filename-extension': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'react/require-default-props': 0,
    'react/forbid-prop-types': 0,
    'no-underscore-dangle': 0, /* annoying for mongoose fields */
    'no-param-reassign': 0,    /* annoying */
    'no-restricted-syntax': 0, /* nudges towards less compatible syntax */
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never',
    }],
  },

  root: true,
};
