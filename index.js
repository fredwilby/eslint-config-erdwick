module.exports = {
  extends: 'airbnb-base',
  env: { node: true, es6: true },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      experimentalObjectRestSpread: true,
    },
  },
    /* airbnb overrides */
  rules: {
    'no-underscore-dangle': 0, /* annoying for mongoose fields */
    'no-param-reassign': 0,    /* annoying */
    'no-restricted-syntax': 0, /* nudges towards less compatible syntax */
    'no-console': 0,
    'no-plusplus': 0,
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never',
    }],
  },

  root: true,
};
